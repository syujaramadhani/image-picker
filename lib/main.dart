import 'package:flutter/material.dart';
import 'package:test_image_picker/splash.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Image Picker Demo',
      home: Splash(),
    );
  }
}
